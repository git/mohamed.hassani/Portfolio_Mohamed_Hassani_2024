Mohamed Hassani UCA - IUT Clermont-Ferrand

# Évaluation portfolio BUT 3A

Mon portfolio a été conçu pour mettre en avant mes compétences techniques et professionnelles
acquises durant mon BUT en informatique. J'ai structuré mon portfolio autour de la section «
Réalisations », en y incluant deux projets SAE majeurs. Le premier projet, AthletiX, est une
application mobile développée en Flutter pour les sportifs, intégrant des exercices de musculation
et des fonctionnalités de partage d'expérience. Le second projet, R-Dash, développé en React-
Native, permet aux pilotes de course de consulter et comparer les données de performance de
leurs voitures. J'ai choisi de mettre en valeur ces projets pour illustrer ma capacité à développer
des solutions complètes et innovantes et à travailler en équipe sur des projets techniques
complexes. Mon portfolio inclut également des sections « À propos de moi » et « CV », où je
présente mon parcours académique et professionnel, ainsi que mes stages et centres d’intérêt,
reflétant ainsi ma polyvalence et mon engagement dans le domaine de l'informatique.

Les compétences développées au cours de mon BUT en informatique sont essentielles pour mon
intégration dans la vie active. Par exemple, la compétence 1 (réaliser un développement
d’application) est illustrée par mes projets AthletiX et R-Dash, où j'ai analysé les besoins des
utilisateurs et conçu des solutions adaptées, comme une application multifonctionnelle pour
sportifs et une application de visualisation de données de course. Cette compétence me sera utile
dans le monde professionnel en me permettant de créer des applications robustes et répondant
aux besoins des utilisateurs. La compétence 6 (collaborer au sein d’une équipe informatique) est
démontrée par ma capacité à travailler en équipe sur R-Dash, développant des compétences en
communication et gestion de projet. Cette compétence est cruciale pour réussir dans un
environnement professionnel, où le travail en équipe et la coordination sont essentiels pour
mener à bien des projets complexes. En outre, mon expérience en stage et en alternance, où j'ai
été responsable en tant que seul développeur sur un système LIMS utilisé par des scientifiques,
m'a permis de développer des compétences en autonomie, en prise de décision, et en gestion de
projet. Ces expériences m'ont préparé à assumer des rôles de développeur logiciel dans des
environnements professionnels diversifiés. Mon projet professionnel est de rejoindre une
entreprise innovante où je pourrais continuer à développer des applications mobiles ou des
systèmes de gestion de données, en appliquant les compétences et les connaissances acquises
tout au long de ma formation. Mon portfolio reflète cette ambition en mettant en avant des
projets concrets et des expériences pertinentes, démontrant ainsi ma préparation et ma capacité
à réussir dans ce domaine.
